const fs = require('fs');
const path = require('path');
const readline = require('readline');

const gas = 'gas';
const water = 'water';

const result = {};

let countFile = 2;

const gasFile = path.join(__dirname, './audit/gas.csv');
const waterFile = path.join(__dirname, './audit/water.csv');

const isInputFileExist = (dataFile) => {
    // Проверка наличия файла - пока не реализована...
    return true;
    /*
    let isFileExist = true;
    fs.access(dataFile, function(error) {
        if (error) {
            isFileExist = false;
        }
    });
    return isFileExist;

     */
}

const addDataInResult = (dataFile) => {

    if (isInputFileExist(dataFile)) {

        const rl = readline.createInterface({
            input: fs.createReadStream(dataFile)
        });

        rl.on('line', (line) => {

            let lineDataArray = line.split(',');
            let dataName  = lineDataArray[2];
            let dataValue = lineDataArray[1];
            let dataDate  = lineDataArray[0];

            if (dataName in result) {
                result[dataName].payList.push({payDate: dataDate, payValue: dataValue});
                result[dataName].payCount = result[dataName].payCount + 1;
            } else {
                result[dataName] = {
                    payName: dataName,
                    payCount: 1,
                    payList: [{payDate: dataDate, payValue: dataValue}],
                }
            }

        });

        rl.on('close', () => {
            countFile = countFile - 1;
        });

    } else {
        console.log(`File ${dataFile} is NOT exist`);
        countFile = countFile - 1;
    }

}


const outputResult = () => {
    if (countFile > 0) {
        setTimeout( outputResult, 500 );
        return;
    }

    for (let payer in result) {
        console.log(`User ${result[payer].payName} - внес ${result[payer].payCount} раз данные`);
        result[payer].payList.forEach( (payItem) => {
            console.log(`${payItem.payDate} - ${payItem.payValue}`);
        });
    }

}



addDataInResult(gasFile);
addDataInResult(waterFile);

outputResult();

