const readline = require('readline');
const path = require('path');

const rl = readline.createInterface( {
    output: process.stdout,
    input: process.stdin,
});

/*
let data = {
    type: 'water || gas',
    value: '000432',
    date: new Date('07-13-2021'),
    name: 'Dmitry'
};
*/


function askSync(q) {
    return new Promise( (resolve, reject) => {
        rl.question(q, (data) => {
            console.log(data, 'received');
            if (false)  // some validations
            {
                reject(new Error('not valided'))
            }
            resolve(data);
        });
    });
}


async function asker() {
    let name = await askSync('What is your name?');
    let data = await askSync('What is data?');
    let type = await askSync('What is type (1 - water, 2 - gas)');
    if (type === '1') type = 'water';
    if (type === '2') type = 'gas';
    let value = await askSync('What is value?');

    const fs = require('fs');
    //const fileName = path.join(__dirname, `./dist/${Date.now()}-info.json`);

    const fileName = path.join(__dirname, `./dist/${Date.now()}-info.json`);
    //const fileName = `dist/${Date.now()}-info.json`;

    let outputJson = JSON.stringify({name: name, date: data, type: type, value: value});

    //console.log('filename - ' + fileName);

    //fs.appendFileSync(fileName, outputJson);

    fs.writeFile(fileName, outputJson, (fileName) => console.log('written file - ' + fileName));



    //console.log('name - ' + name);
    //console.log('data - ' + data);

    rl.close();

}

asker();

/**
rl.question('How are you?', (data) => {
    console.log(data, 'received');
    rl.close();
})

 */